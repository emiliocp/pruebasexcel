# README #

Este archivo contiene información acerca de las pruebas realizadas en PHP con Excel.

### Enlace de la documentación oficial ###

* [Enlace](https://phpspreadsheet.readthedocs.io/en/develop/)

### Configuración del entorno ###

* Paso 1: Instalación de un servidor Apache.
* Paso 2 [Linux]: Instalación de paquetes de php necesarios: php-zip, php-xml, php-mbstring y php-gd (Vienen indicados en la página).
* Paso 3: Iniciar el servidor xampp o lampp con apache.
* Paso 4: Localizar el proyecto en localhost a través de la url.