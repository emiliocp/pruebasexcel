<html>
	<head>
		<title>Escribir en Excel</title>
	</head>
	<body>
		<?php

		require '../vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		$writer = new Xlsx($spreadsheet);
		$writer->save('hello world.xlsx');
		?>
		<a href="http://localhost/pruebasexcel/">Volver</a>
	</body>
</html>