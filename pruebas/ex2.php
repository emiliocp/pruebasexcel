<?php
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if($_SERVER['REQUEST_METHOD']=='POST'){


	$cell = $_POST["cell"];
	$filename = $_POST["name"];
	$texto = $_POST["text"];

	$spreadsheet = new Spreadsheet();
	$spreadsheet->getActiveSheet()->setCellValue($cell, $texto);

	$writer = new Xlsx($spreadsheet);
	$writer->save($filename);

    header('Content-Description: File Transfer');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-disposition: attachment; filename='. $filename);
    header('Content-Length: '.strlen($texto));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Expires: 0');
    header('Pragma: public');
    readfile($filename); // manda el fichero a descargar
    unlink($filename); // borra el fichero creado
    exit;
}
?>
<html>
	<head>
		<title>Ejemplo 2</title>		
	</head>
	<body>
		<form action="" method="POST">
		  <p><label>Nombre del fichero: </label><input type="text" name="name" value="ejemplo.xlsx"></p>
		  <p><label>Celda a escribir: </label><input type="text" name="cell" value="A3"></p>
		  <p><textarea name="text"></textarea></p>
		  <p><input type="submit" value="Download"></p>
		</form>

		<a href="http://localhost/pruebasexcel/">Volver</a>
	</body>
</html>