<?php
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

if($_SERVER['REQUEST_METHOD']=='POST'){
	$target_file = basename($_FILES["filename"]["name"]);
	$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	if(isset($_POST["submit"])) {
		move_uploaded_file($_FILES["filename"]["tmp_name"], $target_file);
		//echo "Exito al almacenar el fichero ". $target_file;

		// Si no se sabe la extension del fichero, se crea un lector automatico
		/*
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		*/

		$reader = new Xlsx();
		$spreadsheet = $reader->load($target_file); //carga el fichero

		$arrayA = $spreadsheet->getActiveSheet()->rangeToArray(
			'A1:B5',     // The worksheet range that we want to retrieve
	        "",        // Value that should be returned for empty cells
	        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
	        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
	        TRUE         // Should the array be indexed by cell row and cell column	
		);

		contentFile($arrayA); //muestra el contenido del fichero

		unlink($target_file); //borra el fichero
	}
	else {
		echo "Error al almacenar el fichero ". $target_file;	
	}
}

?>
<html>
	<head>
		<title>Ejemplo 4</title>		
		<style>
			table {
			    font-family: arial, sans-serif;
			    border-collapse: collapse;
			    width: 100%;
			}

			td, th {
			    border: 1px solid #dddddd;
			    text-align: left;
			    padding: 8px;
			}

			tr:nth-child(even) {
			    background-color: #dddddd;
			}
		</style>
	</head>
	<body>
		<form action="" method="post" enctype="multipart/form-data">
		    Selecciona un fichero Excel:
		    <input type="file" name="filename" id="filename">
		    <input type="submit" value="Subir" name="submit">
		</form>

			<?php 
				function contentFile($arrayF){
					$arrayTitulos = array();
					$arrayCeldas = array();			
					$arrayTitulos[0] = ""; // para que no escriba nada en la esquina superior izquierda

					foreach ($arrayF as $fila => $vector) {
						array_push($arrayCeldas, $vector);
						foreach ($vector as $col => $valor) {
							$arrayTitulos[$col] = $col;						
						}						
					}

					printTable($arrayTitulos, $arrayCeldas);
				}

				function printTable($arrayTitulos, $arrayCeldas){			
					echo "<table>";
						echo "<tr>";
						foreach($arrayTitulos as $k => $v){
							echo "<th>". $v ."</th>";
						}
						echo "</tr>";
						
						foreach($arrayCeldas as $num => $arfil){							
							echo "<tr><td>". ($num + 1) ."</td>";

							foreach($arfil as $col => $v){
								echo "<td>". $v ."</td>";
							}
							echo "</tr>";
						}
						
					echo "</table>";
					
				}

			?>


		<a href="http://localhost/pruebasexcel/">Volver</a>	
	</body>
</html>