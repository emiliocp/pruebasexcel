<?php
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if($_SERVER['REQUEST_METHOD']=='POST'){
	$target_file = basename($_FILES["filename"]["name"]);
	$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	if(isset($_POST["submit"])) {
		if($fileType != 'xlsx'){
			echo "El fichero tiene que ser de tipo xlsx";
		}
		else {
			if(file_exists($target_file)){
				echo "El fichero ". $target_file ." ya existe";
			} else {
				if(move_uploaded_file($_FILES["filename"]["tmp_name"], $target_file)){
		    		echo "Exito al almacenar el fichero ". $target_file;
		    	}
		    	else {
		    		echo "Error al almacenar el fichero ". $target_file;	
		    	}
			}
		}		
	}
}

?>
<html>
	<head>
		<title>Ejemplo 3</title>
	</head>
	<body>
		<form action="" method="post" enctype="multipart/form-data">
		    Selecciona un fichero Excel:
		    <input type="file" name="filename" id="filename">
		    <input type="submit" value="Subir" name="submit">
		</form>

		<a href="http://localhost/pruebasexcel/">Volver</a>	
	</body>
</html>